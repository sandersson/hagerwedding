import {
  Component,
  OnInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

import * as firebase from 'firebase/app';
import { AppState } from '../app.service';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
  selector: 'admin',  // <login></login>
  templateUrl: './admin.component.html',
})
export class AdminComponent implements OnInit {
  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;
  invites: any[]

  @ViewChild('hoolaaa') firstNameElement: ElementRef;

  constructor(
    public appState: AppState,
    public afAuth: AngularFireAuth,
    public db: AngularFireDatabase
  ) {
    this.itemsRef = db.list('invites')
    this.items = this.itemsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
    
    this.items.subscribe( value => {
      this.invites = value
    })
  }

  public ngOnInit() {
  
  }

  checkCode (code: string): boolean {
    let isUnique = true
    Object.keys(this.invites).forEach((i) => {
      if (this.invites[i].code === code)
      isUnique = false
    })

    return isUnique
  }

  addInvite(f: NgForm) {
    console.log('submit')
    if (f.valid) {

      var rString = ''
      let isUnique = false
      do {
        rString = this.randomString(6, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
        isUnique = this.checkCode(rString)
      } while (!isUnique);
      
      f.value.code = rString
      this.itemsRef.push(
        f.value
      );

      f.reset()
      this.firstNameElement.nativeElement.focus()
      console.log(this.firstNameElement)
    }
  }

  randomString(length: number, chars: string): string {
    var result = '';
    for (var i = length; i > 0; --i) {
      result += chars[Math.floor(Math.random() * chars.length)]
    }
    return result;
  }

  updateItem(key: string, firstName: string, lastName: string, email: string, plusFirstName: string, plusLastName: string, code: string) {
    console.log('update')
    this.itemsRef.update(key, {
      firstName: firstName,
      lastName: lastName,
      email: email,
      plusFirstName: plusFirstName,
      plusLastName: plusLastName,
      code: code
    });
  }
  deleteItem(key: string) {    
    this.itemsRef.remove(key); 
  }
  deleteEverything() {
    this.itemsRef.remove();
  }

  
  login(f: NgForm) {
    if (f.valid) {
      this.afAuth.auth.signInWithEmailAndPassword(f.value.email, f.value.password)
    }
  }
  
  logout() {
    this.afAuth.auth.signOut();
  }
}
