import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { NgForm } from '@angular/forms'
import { Guest } from '../models/guest'
import { Invite } from '../models/invite'
import { AppState } from '../app.service'
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database'
import { Observable } from 'rxjs/Observable'
import * as firebase from 'firebase/app'
import { AngularFireObject } from 'angularfire2/database/interfaces';
import { access } from 'fs';

interface Songs {
  one?: string
  teenage?: string
}

@Component({
  selector: 'osa',
  templateUrl: './osa.component.html'
  
})
export class OsaComponent implements OnInit {
  rsvp: string
  showAlcohol = false
  canHavePlusOne = true
  greeting: string
  yesRef: AngularFireObject<any>
  noRef: AngularFireObject<any>
  noObservable: Observable<any[]>;
  yesObservable: Observable<any[]>;
  yesItems: any
  noItems: any
  done: boolean
  rsvpDone: boolean
  songs: Songs = {}
  invite: Invite
  update: boolean

  constructor(
    public appState: AppState,
    public db: AngularFireDatabase
  ) {
    this.yesRef = db.object('yes')
    this.noRef = db.object('no')
    this.yesObservable = this.yesRef.snapshotChanges().map(action => {
      return action.payload.val()
    });
    this.noObservable = this.noRef.snapshotChanges().map(action => {
      return action.payload.val()
    });

    this.yesObservable.subscribe( value => {
      this.yesItems = value
    })

    this.noObservable.subscribe( value => {
      this.noItems = value
    })

  }

  guests: Guest[] = []

  public ngOnInit() {
    this.invite = this.appState.state.invite
    console.log('appstate invite:', this.invite)

    this.guests[0] = new Guest(this.invite.code, this.invite.email, this.invite.firstName, this.invite.lastName)
    if (this.invite.plusFirstName) {
      this.guests[1] = new Guest(this.invite.code, this.invite.email, this.invite.plusFirstName, this.invite.plusLastName)
    }
    
    console.log(this.guests)
  
    

  }

  yes () {
    this.rsvp = 'yes'
    for (let guest of this.guests) {
      guest.rsvp = true
    }
  }

  changeShowAlcohol() {
    this.showAlcohol = !this.showAlcohol
  }
  no () {
    this.rsvp = 'no'
    for (let guest of this.guests) {
      guest.rsvp = false
    }
  }

  onYesSubmit () {
    const inviteCode = this.guests[0].inviteCode
    // Remove from no list
    if (this.noItems && this.noItems[inviteCode]) {
      let save = {}
      save[inviteCode] = {}
      save[inviteCode+'+1'] = {}
      this.noRef.update(save)
    }

    let save = {}
    save[this.guests[0].inviteCode] = this.filterFalsyValues(this.guests[0])
    if (this.guests[1]) {
      save[this.guests[0].inviteCode+'+1'] = this.filterFalsyValues(this.guests[1])
    }
    this.yesRef.update(save)

    this.done = true
  }

  onNoSubmit () {
    const inviteCode = this.guests[0].inviteCode
    // Remove from yes list
    if (this.yesItems && this.yesItems[inviteCode]) {
      let save = {}
      save[inviteCode] = {}
      save[inviteCode+'+1'] = {}
      this.yesRef.update(save)
    }

    let save = {}
    for (let i of this.guests) {
      save[inviteCode] = this.filterFalsyValues(i)
    }
    this.noRef.update(save)

    this.rsvpDone = true
  }

  plusOne () {
    console.log('plusOne!')
    this.guests[1] = new Guest(this.guests[0].inviteCode, this.guests[0].email)
  }
  
  filterFalsyValues (o: object): object {
    Object.keys(o).forEach(k => {
      if (!o[k] || o[k] === undefined) {
        delete o[k]
      }
    })
    return o
  }
  
  onSongSubmit (): void {
    console.log(this.songs)

    const bestSongRef = this.db.list('bestSongEver')
    const teenageSong = this.db.list('teenageSongs')
    bestSongRef.push(this.songs.one)
    teenageSong.push(this.songs.teenage)

    this.rsvpDone = true
    
  }

  updateRSVP (): void {
    this.done = false
    this.rsvpDone = false
    this.update = true
  }



}
