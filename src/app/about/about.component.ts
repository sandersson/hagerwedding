import {
  Component,
  OnInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Invite } from '../models/invite'
import { AppState } from '../app.service';

@Component({
  selector: 'about',
  templateUrl: './about.component.html'
  
})
export class AboutComponent implements OnInit {

  public invite: Invite
  constructor(
    public route: ActivatedRoute,
    public appState: AppState
  ) {}

  public ngOnInit() {
    this.invite = this.appState.state.invite
  }


}
