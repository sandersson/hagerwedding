import { Routes } from '@angular/router';
import { LoginComponent } from './login';
import { AdminComponent } from './admin'
import { AboutComponent } from './about';
import { OsaComponent } from './osa'
import { NoContentComponent } from './no-content';

import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '',      component: LoginComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'about', component: AboutComponent },
  { path: 'osa',   component: OsaComponent },
  { path: '**',    component: NoContentComponent },
];
