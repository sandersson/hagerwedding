export class Invite {
  
    constructor(
      public code: string,
      public email: string,
      public firstName: string,
      public lastName: string,
      public plusFirstName?: string,
      public plusLastName?: string,
    ) {  }
  
  }