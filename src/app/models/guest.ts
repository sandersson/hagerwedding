export class Guest {
  
    constructor(
      public inviteCode: string,
      public email: string,
      public firstName?: string,
      public lastName?: string,
      public rsvp?: boolean,
      public greeting?: string,
      public plusOneTo?: string,
      public gluten?: boolean,
      public laktos?: boolean,
      public otherAllergy?: string,
      public alcohol?: boolean,
      public alcoholType?: string
    ) {  }
  
  }