import {
  Component,
  OnInit
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { AppState } from '../app.service';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Jsonp } from '@angular/http/src/http';
import { Router } from '@angular/router';
import anime from 'animejs'

@Component({
  selector: 'login',  // <login></login>
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public localState = { value: '' };
  itemsRef: AngularFireList<any>;
  items: Observable<any[]>;
  invites: any[]

  constructor(
    public appState: AppState,
    public db: AngularFireDatabase,
    public router: Router
  ) {
    this.itemsRef = db.list('invites')
    this.items = this.itemsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    });
    
    this.items.subscribe( value => {
      this.invites = value
      // console.log(value)
    })
  }


  public ngOnInit() {
    console.log('appstate invite:', this.appState.state.invite)

    setTimeout(() => {
      anime.timeline()
      .add({
        targets: '.ml5 .line',
        opacity: [0.5,1],
        scaleX: [0, 1],
        easing: "easeInOutExpo",
        duration: 700
      }).add({
        targets: '.ml5 .line',
        duration: 600,
        easing: "easeOutExpo",
        translateY: function(e, i, l) {
          var offset = -0.625 + 0.625*2*i;
          return offset + "em";
        }
      }).add({
        targets: '.ml5 .ampersand',
        opacity: [0,1],
        scaleY: [0.5, 1],
        easing: "easeOutExpo",
        duration: 600,
        offset: '-=600'
      }).add({
        targets: '.ml5 .letters-left',
        opacity: [0,1],
        translateX: ["0.5em", 0],
        easing: "easeOutExpo",
        duration: 600,
        offset: '-=300'
      }).add({
        targets: '.ml5 .letters-right',
        opacity: [0,1],
        translateX: ["-0.5em", 0],
        easing: "easeOutExpo",
        duration: 600,
        offset: '-=600'
      }).add({
        targets: '.login__date',
        opacity: [0,1],
        easing: "easeOutExpo",
        duration: 600
      }).add({
        targets: '.login__wrap',
        opacity: [0,1],
        easing: "easeOutExpo",
        duration: 600
      })
    }, 500)

    
  }

  onSubmit(f: NgForm) {
    // console.log(f.value)
    if (f.valid) {
      let invite = this.getInvite(f.value.code)
      if (invite) {
        this.appState.set('invite', invite)
        localStorage.setItem('invite', JSON.stringify(invite) )
        this.router.navigate(['/about']);
      }
    }
  }

  getInvite (code: string): any {
    let invite = undefined
    Object.keys(this.invites).forEach((i) => {
      if (this.invites[i].code === code)
      invite = this.invites[i]
    })

    return invite
  }

  public submitState(value: string) {
    // console.log('submitState', value);
    this.appState.set('value', value);
    this.localState.value = '';
  }
}
